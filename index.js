console.log ("Welcome trainers to Coding Gym");
console.log ("***one pokemon to rule them all***".toUpperCase());


let trainer = {
	name: "Ash Ketchum",
	age: 16,
	pokemon:[
		"Pikachu",
		"Chikorita",
		"Cyndaquil",
		"Totodile",
		"Entei",
		"Celebi"
	], 
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!!!")
	}
}
console.log(trainer);
console.log("Result of dot notation");
console.log(trainer.name);
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();

function Pokemon (name, level){
	this.name = name;
	this.level = level;
	this.health = 2.5*level; 
	this.attack = 1.5*level; 
	this.talk = function(target){
		console.log (target.name + "I choose you")
	};
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
	};
	this.faint = function(){
		console.log(this.name + " has fainted.")
	};
}
}

let Pikachu = new Pokemon ("Pikachu", 12);
console.log(Pikachu);

let Chikorita = new Pokemon ("Chikorita", 10);
console.log(Chikorita);

let Cyndaquil = new Pokemon ("Cyndaquil", 9);
console.log(Cyndaquil);

let Totodile = new Pokemon ("Totodile", 18);
console.log(Totodile);

let Entei = new Pokemon ("Entei", 12);
console.log(Entei);

let Celebi = new Pokemon ("Celebi", 2);
console.log(Celebi);

Pikachu.tackle(Celebi);
console.log(Celebi)
Chikorita.tackle(Pikachu);
console.log(Pikachu);
Cyndaquil.tackle(Pikachu);
console.log(Pikachu);
Totodile.tackle(Pikachu);
console.log(Pikachu);
Totodile.tackle(Chikorita);
console.log(Chikorita)
Totodile.tackle(Entei);
console.log(Entei);
Entei.tackle(Totodile);
console.log(Totodile);
Totodile.tackle(Entei);
console.log(Entei);
Totodile.tackle(Cyndaquil);
console.log(Cyndaquil);

console.log("We have a winner!");




